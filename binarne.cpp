
// DRZEWO BINARNE //
#include "stdafx.h"
#include  <stdlib.h>
#include  <stdio.h>
#include  <string.h>
#include <iostream>
#include <windows.h>
#include <ctime>
using namespace std;

class Statyczna //Do liczenia wysokosci
{
public:
	static int h;
	int printh(){
		return h;
	}
	void  Stat(int i) {
		if (h < i) {
			h = i;

		}
	}
	void destroy()
	{
		h = 0;
	}
};
int Statyczna::h = 0;
Statyczna o;


struct W{ // Wezel
public:
	int h=0;
	int value;
	struct W *l;
	struct W *p;

}*root = NULL;

void push(struct W *&root, int x) {
	if (root == NULL) {
		root = (struct W*)malloc(sizeof(struct W));
		root->l = NULL;
		root->p = NULL;
		root->value=x;
		return;
	}
	else {
		if (x < root->value) 
		{		//dodaje na lewo	
			root->o = root;
			push(root->l, x);
		}		
		else {			//dodaje na prawo
			root->o = root;
			push(root->p, x);
		}
	}
}

bool del(struct W *&root, int x)
{
	if (root)
	{
		if (root->value == x) {
			return 1;
		}
	}
	if (root->l) {
		if (del(root->l, x) == 1)
		{
			root->l = NULL;
			return 0;
		}
	}	
	if (root->p) {
		if (del(root->p, x) == 1)
		{
			root->p = NULL;
			return 0;
		}
	}
	return 0;
}

void height(struct W *root, int x)
{
	if (root)
	{		
		++x;
		o.Stat(x);//zapisuje wysokosc jezeli jest wyzsza niz znana dotychczas
		height(root->l, x);
		height(root->p, x);
	}
}

void LiczWysokosc(struct W *root, int x)
{
	o.destroy();//czysci wysokosc
	height(root, x);//liczy od nowa
}


void printt(struct W *root, int x) //Wypisuje drzewo od lewa, czyli od najmniejszych liczb
{
	if (root)
	{
		++x;		
		printt(root->l, x);
		for (int i = 0; i<=x; i++)
		{
			cout << " - ";
		}
		cout << "|{ " << root->value << " }"<<endl;
		printt(root->p, x);
	}
}


int main()
{
	
	time_t tt;
	srand(time(&tt));
	int n, i, x, y;
	cout << "Do drzewa dodaje losowa ilosc elementow w zakresie od 3 do 10, oraz o losowych wartosciach (1-10)" << endl;
	n = rand() % 10 + 6;
	for (i = 0; i<n; i++)
	{
		x = rand() % 15 + 1;
		cout << "Dodano: "<<x << endl;
		push(root, x);
	}
	LiczWysokosc(root, 0);
	cout <<"\n Aktualna wysokosc drzewa:  "<< o.printh() <<endl;
	printt(root, 0);
	cout << " \n Podaj wartosc do usuniecia, zostanie usunieta cala galaz :  ";
	cin >> y;
	del(root, y);
		printt(root, 0);
		LiczWysokosc(root, 0);
		cout << "\n Aktualna wysokosc drzewa:  " << o.printh() << endl;
	cout << '\n';
	system("PAUSE");

}