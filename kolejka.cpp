// ConsoleApplication10.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <iostream>
#include <ctime>
using namespace std;

struct element {
	element* next;
	element* prev;
	int val, prio;
};

class kolejka {
private:
	element *head;
	element *tail;
public:
	kolejka();
	bool empty();
	void push(int prio, int val);
	void print();
	int removeMin();
};


kolejka::kolejka() {
	head = tail = NULL;
}
bool kolejka::empty()
{
	return !head;
}
int kolejka::removeMin() {

	if (head)
	{
		element * a = head;
		head = head->next;
		if (!head) tail = NULL;
		return a->val;
	}
}
void kolejka::print()
{
	element *temp = head;
	int i = 1;

	if (temp == 0)
		cout << "LISTA PUSTA";
	while (temp) {
		cout << "\n Element #" << i++ << " dana = " << temp->val << " Prio = " << temp->prio << endl;
		temp = temp->next;
	}
	cout << endl;
}
void kolejka::push(int prio,int val) {
	element *a=NULL, *b=NULL;
	a = new element;
	a->next = NULL;
	a->prev = NULL;
	a->prio = prio;
	a->val = val;
	
	if (!head) { //jezeli kolejka jest pusta
		head = tail = a;
	}
	else if (head->prio > prio) // jezeli priorytet jest wiekszy niz headzie
	{
		a->next = head;

		head = a;
	}
	else {
		b = head;
		while ((b->next) && (b->next->prio <= prio)) {
			b = b->next;
		}
		a->next = b->next;
		b->next = a;
		if (!a->next)
		{
			tail = a;
		}

	}
}




int main()
{
	time_t tt;
	srand(time(&tt));
	int p, v;
	kolejka k;
	for (int i = 0;  i != 6; ++i) {
		//cin >> v;
		//p = v;
		v = rand() % 100;
		p = rand() % 100;
		cout << "\n \n Wprowadzona dana: " << v << endl << "  o priorytecie: " << p<<endl;

		k.push(p, v);
	}
	cout << "\n Wyswietlam kolejke: ";
	k.print();
	cout << "\n Usuwam element o najmniejszym priorytecie: ";
    cout<<k.removeMin();
	k.print();

		cin >> p;


    return 0;
}

